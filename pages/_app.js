import store from '../store/store';
import '../styles/globals.css';
import { Provider } from 'react-redux';
// ReactModal.setAppElement('root');
function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
