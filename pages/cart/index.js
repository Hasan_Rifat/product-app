import CartItems from "../../components/subpages/cart";

const cart = () => {
  return (
    <div>
      <CartItems/>
    </div>
  );
};

export default cart;