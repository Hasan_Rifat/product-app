import Home from '../components/subpages/Home';
import { useDispatch } from 'react-redux';
import { allProducts } from '../store/dealProductSlice';
import { useEffect } from 'react';

export default function HomeIndex({ products }) {
  // console.log(products);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allProducts(products));
  }, [products, dispatch]);

  return (
    <div className=''>
      <Home />
    </div>
  );
}

export async function getServerSideProps() {
  const response = await fetch(
    'https://next-js-server-puce.vercel.app/api/dealProduct'
  );
  const data = await response.json();

  return {
    props: {
      products: data,
    },
  };
}
