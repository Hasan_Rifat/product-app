import { configureStore } from '@reduxjs/toolkit';
import dealProductsSlice from './dealProductSlice';
import shopProductsSlice from './shopProductSlice';

const store = configureStore({
  reducer: {
    dealProductsReducer: dealProductsSlice,
    shopProductsReducer: shopProductsSlice,
  },
});

export default store;
