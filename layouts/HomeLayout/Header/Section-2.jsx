import { Transition } from '@headlessui/react';
import { useState } from 'react';
import { AiFillCaretDown } from 'react-icons/ai';
import { BsPercent } from 'react-icons/bs';
import { BiChevronDown } from 'react-icons/bi';
import { CgMenuLeft } from 'react-icons/cg';
import Link from 'next/link';

const SectionTwo = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div>
      <nav className='bg-white'>
        <div className=' container mx-auto'>
          <div className='flex items-center justify-between h-16'>
            <div className='flex items-center'>
              <div className='flex-shrink-0 bg-[#27AE60] smd:px-16 px-6 py-2 smd:py-2.5 rounded-full'>
                <div className='flex items-center justify-between'>
                  <Link href={'/test'}>
                    <a>
                      <i className='text-white font-medium'>
                        <CgMenuLeft />
                      </i>
                    </a>
                  </Link>
                  <h1 className='text-white xs:text-md text-[16px]'>All Category</h1>
                  <Link href={'/test'}>
                    <a>
                      <i className='text-white text-2xl font-medium'>
                        <BiChevronDown />
                      </i>
                    </a>
                  </Link>
                </div>
              </div>
              <div className='hidden lg:block ml-10'>
                <div className='ml-16 flex items-baseline space-x-2'>
                  <Link href={'/'}>
                    <a className='text-[#4F4F4F] px-3 py-2 rounded-md text-sm flex items-center font-medium'>
                      Home
                      <i className='ml-1'>
                        <AiFillCaretDown />
                      </i>
                    </a>
                  </Link>

                  <Link href={'/shop'}>
                    <a className='text-[#4F4F4F] flex items-center px-3 py-2 rounded-md text-sm font-medium'>
                      Shop
                      <i className='ml-1'>
                        <AiFillCaretDown />
                      </i>
                    </a>
                  </Link>

                  <Link href={'/test'}>
                    <a className='text-[#4F4F4F] flex items-center px-3 py-2 rounded-md text-sm font-medium'>
                      Pages
                      <i className='ml-1'>
                        <AiFillCaretDown />
                      </i>
                    </a>
                  </Link>

                  <Link href={'/test'}>
                    <a className='text-[#4F4F4F] flex items-center px-3 py-2 rounded-md text-sm font-medium'>
                      Blog
                      <i className='ml-1'>
                        <AiFillCaretDown />
                      </i>
                    </a>
                  </Link>

                  <Link href={'/test'}>
                    <a className='text-[#4F4F4F] px-3 py-2 rounded-md text-sm font-medium'>
                      Contact
                    </a>
                  </Link>
                  <Link href={'/test'}>
                    <a className='text-[#4F4F4F] px-3 py-2 rounded-md text-sm font-medium lg:hidden xl:block'>
                      Track Order
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className='flex items-center justify-around relative'>
              <i className='text-[#FF805D] px-3 font-medium hidden xs:block'>
                <BsPercent />
              </i>
              <Link href={'/test'}>
                <a className='text-[#FF805D] font-medium hidden xs:block'>Special Offers!</a>
              </Link>
            </div>
            <div className='-mr-2 flex lg:hidden'>
              <button
                onClick={() => setIsOpen(!isOpen)}
                type='button'
                className='bg-gray-900 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white'
                aria-controls='mobile-menu'
                aria-expanded='false'
              >
                <span className='sr-only'>Open main menu</span>
                {!isOpen ? (
                  <svg
                    className='block h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                    aria-hidden='true'
                  >
                    <path
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      strokeWidth='2'
                      d='M4 6h16M4 12h16M4 18h16'
                    />
                  </svg>
                ) : (
                  <svg
                    className='block h-6 w-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                    aria-hidden='true'
                  >
                    <path
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      strokeWidth='2'
                      d='M6 18L18 6M6 6l12 12'
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>

        <Transition
          show={isOpen}
          enter='transition ease-out duration-100 transform'
          enterFrom='opacity-0 scale-95'
          enterTo='opacity-100 scale-100'
          leave='transition ease-in duration-75 transform'
          leaveFrom='opacity-100 scale-100'
          leaveTo='opacity-0 scale-95'
        >
          {(ref) => (
            <div className='md:hidden' id='mobile-menu'>
              <div ref={ref} className='px-2 pt-2 pb-3 space-y-1 sm:px-3'>
                <Link href={'/test'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Home
                  </a>
                </Link>

                <Link href={'/shop'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Shop
                  </a>
                </Link>

                <Link href={'/test'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Pages
                  </a>
                </Link>

                <Link href={'/test'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Blogs
                  </a>
                </Link>

                <Link href={'/test'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Contact
                  </a>
                </Link>
                <Link href={'/test'}>
                  <a className='text-[#4F4F4F] block px-3 py-2 rounded-md text-base font-medium'>
                    Track Order
                  </a>
                </Link>
              </div>
            </div>
          )}
        </Transition>
      </nav>
    </div>
  );
};
export default SectionTwo;
