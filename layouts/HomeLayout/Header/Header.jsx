import { MdOutlineShoppingBag } from 'react-icons/md';
import { HiOutlineSearch } from 'react-icons/hi';
import { AiOutlineHeart } from 'react-icons/ai';
import account from '../../../public/images/Icon.png';
import Image from 'next/image';
import SectionTwo from './Section-2';
import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';

const Navbar = () => {
  const Carts = useSelector((state) => state.dealProductsReducer.cart);
  // console.log(numOfCart);
  return (
    <header className='py-4 bg-white px-3'>
      <div className='container mx-auto flex items-center md:justify-between flex-col md:flex-row'>
        {/*-- logo --*/}
        <div className='flex basis-2/3 items-center justify-between'>
          <div className='basis-2/5 smd:basis-2/5'>
            <Link href={'/'}>
              <a
                href='#'
                className='smd:text-[25px] xs:text-md text-md font-bold uppercase'
              >
                Bengal shop
              </a>
            </Link>
          </div>
          {/*-- searchbar --*/}
          <div className='w-full max-w-xl relative flex mt-3 md:mt-0 items-center basis-2/3 smd:basis-1/2 md:basis-2/5 lg:basis-1/2 md:-left-[15%] '>
            <span className='absolute top-1 text-md smd:text-xl text-gray-700 px-3 py-1'>
              <i>
                <HiOutlineSearch />
              </i>
            </span>
            <input
              type='text'
              className=' xs:w-11/12 md:w-full w-full border border-[#E0E0E0] border-r-0 pl-9 smd:py-2 py-1 xs:py-2 pr-3 rounded-full focus:outline-none text-sm '
              placeholder='Search here...'
            />

            <button className='bg-[#333333] text-white smd:text-[18px] text-[12px] border-gray-400 px-6 xs:px-8 rounded-full hover:text-gray-400 transition absolute md:ml-[80%] ml-[50%] md:py-1.5 p-1.5 xs:p-2'>
              Search
            </button>
          </div>
        </div>
        {/*-- icons --*/}
        <div className='flex item-center space-x-[32px] mt-7 md:mt-0'>
          <a
            href='#'
            className='text-center text-gray-700 hover:text-primary transition relative mt-4'
          >
            <div className='text-2xl md:text-xl lg:text-2xl md:ml-8 '>
              <i>
                <AiOutlineHeart />
              </i>
            </div>
          </a>
          <Link href={'/cart'}>
            <a className='text-center text-gray-700 hover:text-primary transition relative mt-4'>
              <div className='text-2xl'>
                <i>
                  <MdOutlineShoppingBag />
                </i>
              </div>
              <span className='absolute -right-4 -top-5 w-5 h-5 rounded-full items-center justify-center bg-red-500 text-white text-sm'>
                {Carts.length}
              </span>
            </a>
          </Link>
          <a
            href='#'
            className='text-center text-gray-700 hover:text-primary transition relative flex items-center'
          >
            <div className='text-4xl bg-[#27AE60] w-[50px] h-[50px] rounded-full'>
              <Image
                src={account}
                alt='image not found'
                className='object-cover rounded'
              ></Image>
            </div>
            <div className='acc ml-3'>
              <p className='text-[18px]'>Account</p>
            </div>
          </a>
        </div>
      </div>

      <SectionTwo />
    </header>
  );
};

export default Navbar;
