import Image from 'next/image';
import cs1 from '../../../public/images/logo.PNG';

const Footer = () => {
  return (
    <footer className='text-center lg:text-left bg-[#F5F5F5] text-[#4F4F4F]'>
      <div className=' container mx-auto pt-[105px] text-center md:text-left'>
        <div className='grid grid-1 md:grid-cols-2 lg:grid-cols-3 gap-8'>
          <div className=''>
            
            <h6
              className='
            font-semibold
            mb-4
            flex
            items-center
            justify-center
            md:justify-start
            text-[#333333]
            text-[32px]
          '
            >
              Bengal shop
            </h6>
            <p className=' text-[16px]'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
              diam ornare nam est gravida. Netus viverra rhoncus sit magna
              sapien ac eget parturient id. Est luctus dapibus quam aliquam in
              nisl turpis. Elit et dictum lacus morbi nec accumsan a in.
            </p>
          </div>
          <div className='md:ml-[200px]'>
            <h6 className=' font-semibold mb-4 flex justify-center md:justify-start text-[24px]'>
              About Us
            </h6>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                About Karte
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Contact
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Career
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Terms & Conditions
              </a>
            </p>
            <p className=''>
              <a href='#!' className='text-[#828282]'>
                Category
              </a>
            </p>
          </div>
          <div className='lg:ml-[150px]'>
            <h6 className=' font-semibold mb-4 flex justify-center md:justify-start text-[24px]'>
              Info
            </h6>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                information
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Shipping
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Payment
              </a>
            </p>
            <p className='mb-4'>
              <a href='#!' className='text-[#828282]'>
                Return
              </a>
            </p>
            <p className=''>
              <a href='#!' className='text-[#828282]'>
                Blog
              </a>
            </p>
          </div>
        </div>
      </div>
      <div className='container mx-auto text-center pt-[115px] pb-[67px] grid grid-1 md:grid-cols-2 lg:grid-cols-3 gap-8'>
        <div className=''>Brand logo </div>
        <div className=''>
          <p>@2022 Copyright All Right Reserved by Bengal Shop</p>
        </div>
        <div className=''>payment</div>
      </div>
    </footer>
  );
};

export default Footer;
