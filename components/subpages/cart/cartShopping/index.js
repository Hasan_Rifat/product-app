import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';
import CartDetails from './cartDetails';

const CartShopping = () => {
  const Cart = useSelector((state) => state.dealProductsReducer.cart);

  let total = 0;
  for (let i = 0; i < Cart.length; i++) {
    total += Cart[i].numOfCartProduct * Cart[i].currentPrice;
    // console.log('total',total)
  }

  // console.log(Cart);
  return (
    <div className='container mx-auto'>
      <div className='grid grid-cols-12 gap-5 items-start mt-10 px-4 mb-12'>
        <div className='grid col-span-12 lg:col-span-8 md:col-span-8 xs:col-span-12 px-4'>
          <div className='grid grid-cols-4 p-3 bg-gray-300 rounded-sm text-xl'>
            <div className='grid col-span-2'>
              <p className='smd:text-[20px] text-[16px]'>Product</p>
            </div>
            <div className='grid col-span-1'>
              <p className='ml-3 smd:text-[20px] text-[16px]'>Quantity</p>
            </div>
            <div className='grid col-span-1'>
              <p className='ml-2 smd:text-[20px] text-[16px]'>Total</p>
            </div>
          </div>

          <div className='mt-4'>
            {Cart.map((product) => (
              <CartDetails key={product.id} product={product}></CartDetails>
            ))}
          </div>
        </div>
        <div className='grid md:col-span-4 xs:col-span-12 border col-span-12 border-green-300 p-4 rounded'>
          <h4 className='text-gray-800 smd:text-lg sm:text-md xs:text-[16px] mb-4 font-bold uppercase'>
            Order summary
          </h4>

          <div className='flex justify-between text-gray-800 font-medium my-3 uppercase'>
            <p>Subtotal</p>
            <p>${total}</p>
          </div>
          <div className='flex justify-between text-gray-800 font-medium my-3 uppercase'>
            <p>Delivery</p>
            <p>Free</p>
          </div>
          <div className='flex justify-between border-b border-gray-200 text-gary-800 font-medium my-3 uppercase'>
            <p>Tax</p>
            <p>Free</p>
          </div>
          <div className='flex justify-between text-gray-800 font-medium my-3 uppercase'>
            <p className='font-semibold'>Total</p>
            <p>${total}</p>
          </div>
          <Link href={'/cart/checkout'}>
            <a className='w-full block text-center bg-green-600 text-white p-4 font-medium rounded-md hover:bg-transparent hover:text-green-500 hover:border-2 hover:border-green-400 text-sm md:text-sm lg:text-xl transition'>
              Process To Checkout
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CartShopping;
