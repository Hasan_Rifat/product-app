import { CgMenuGridR } from 'react-icons/cg';
import { HiOutlineMenuAlt3 } from 'react-icons/hi';

import Image from 'next/image';

import cat1 from '../../../../public/images/categories/cat-1.png';
import cat2 from '../../../../public/images/categories/cat-2.png';
import cat3 from '../../../../public/images/categories/cat-3.png';
import cat4 from '../../../../public/images/categories/cat-4.png';
import cat5 from '../../../../public/images/categories/cat-5.png';
import cat6 from '../../../../public/images/categories/cat-6.png';
import cat7 from '../../../../public/images/categories/cat-7.png';

import hero from '../../../../public/images/categories/hero bg.png';

import { useSelector, useDispatch } from 'react-redux';
import ShopProductView from './shopProductView';
import { filterProduct } from '../../../../store/shopProductSlice';

const ShopProduct = () => {
  const products = useSelector(
    (state) => state.shopProductsReducer.copyOfProducts
  );
  // console.log('products of shop', products);

  const dispatch = useDispatch();

  return (
    <div className='container mx-auto'>
      <div className='grid grid-cols-4 gap-6 pt-4 pb-5 items-start px-4'>
        <div
          className='col-span-2 sm:col-span-1 bg-white
          smd:px-5 md:px-10 
          pb-8
          shadow
          rounded
          overflow-hidden'
        >
          <div className='divided-y divided-gray-200 space-y-5'>
            <div className=' p-4'>
              <h3 className='lg:text-xl text-gray-800 mb-3 capitalize font-medium'>
                categories
              </h3>
              <div className='space-y-10 mt-3'>
                <div className='flex items-center '>
                  <Image
                    src={cat1}
                    alt='Landscape picture'
                    width={20}
                    height={20}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('vegetables'));
                    }}
                  >
                    Vegetables
                  </button>
                </div>
                <div className='flex items-center '>
                  <Image
                    src={cat2}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('fruits'));
                    }}
                  >
                    Fruits
                  </button>
                </div>
                <div className='flex items-center'>
                  <Image
                    src={cat3}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('groceries'));
                    }}
                  >
                    Groceries
                  </button>
                </div>
                <div className='flex items-center'>
                  <Image
                    src={cat4}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('meat'));
                    }}
                  >
                    Meat
                  </button>
                </div>
                <div className='flex items-center'>
                  <Image
                    src={cat5}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('fish'));
                    }}
                  >
                    Fish
                  </button>
                </div>
                <div className='flex items-center'>
                  <Image
                    src={cat6}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('bevarage'));
                    }}
                  >
                    Bevarage
                  </button>
                </div>
                <div className='flex items-center'>
                  <Image
                    src={cat7}
                    alt='Landscape picture'
                    width={30}
                    height={30}
                  />
                  <button
                    className='mx-2 lg:text-[18px] md:text-[15px] smd:text-[13px]'
                    onClick={() => {
                      dispatch(filterProduct('dry food'));
                    }}
                  >
                    Dry Food
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='col-span-3'>
          <div className='relative hidden smd:block'>
            <div className='hero w-full h-full '>
              <Image
                // width={1275}
                // height={300}
                // layout='fill'
                src={hero}
                alt='image not found'
                className='object-cover'
              ></Image>
            </div>
            <div className='absolute inset-0 md:top-[30%] left-[5%] smd:top-[10%] space-y-2'>
              <p className=' text-[#FF5C00]'>Buy 1 Get 1</p>
              <div className='font-bold text-[#27AE60]  xl:text-2xl md:text-md smd:text-[15px] smd:font-medium'>
                Up to 30% Discount <br /> on Selected Items
              </div>
            </div>
          </div>
          <div className=''>
            <h1 className='xl:text-2xl md:text-md smd:text-[15px] font-bold capitalize'>
              {products[0]?.category} Collection
            </h1>
            <div className='flex items-center justify-between mb-4'>
              <div className='flex items-center flex-col xl:flex-row space-x-4 space-y-2 mt-4 xl:space-y-0 xl:mt-0 justify-center'>
                <div className='-mt-4'>
                  <p>Fruits</p>
                </div>
                <div className='hidden lg:block'>
                  <p>Green Fruits</p>
                </div>
                <div className='hidden lg:block'>
                  <p>Fresh Fruits</p>
                </div>
              </div>
              <div className='flex items-center justify-between px-8'>
                <div className='hidden md:block'>
                  <p>{products.length} Products Found</p>
                </div>
                <div className='flex gap-2 px-3'>
                  <div className='w-10 h-9 flex items-center  text-xl text-gray-800 rounded cursor-pointer'>
                    <i>
                      <HiOutlineMenuAlt3 />
                    </i>
                  </div>
                  <div className='w-10 h-9 flex items-center text-xl text-gray-500   rounded cursor-pointer'>
                    <i>
                      <CgMenuGridR />
                    </i>
                  </div>
                </div>
                <div className=''>
                  <select className='select w-44 text-sm text-gray-600 px-4 py-3 border-gray-300 shadow-sm rounded focus:ring-green-500 focus:border-green-500'>
                    <option value=''>Default sorting</option>
                    <option value=''>Price low-height</option>
                    <option value=''>Price hight-low</option>
                    <option value=''>Latest product</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className='grid smd:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 grid-cols-1 gap-6'>
            {products.length ? (
              products.map((product) => (
                <ShopProductView
                  key={product.id}
                  product={product}
                ></ShopProductView>
              ))
            ) : (
              <div className='text-xl text-center font-bold text-red-500 grid col-span-full mt-5'>
                <h1>Sorry!! Products not Available</h1>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopProduct;
