import Layout from '../../../layouts';
import ShopProduct from './shopProduct';

const ShopMain = () => {
  return (
    <div className=''>
      <Layout>
        <ShopProduct/>
      </Layout>
    </div>
  );
};

export default ShopMain;
