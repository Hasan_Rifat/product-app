import img3 from '../../../../public/images/grocery_collection.png';
import img1 from '../../../../public/images/fruits_collection.png';
import img2 from '../../../../public/images/vegetable_collection.png';

import { BsArrowRightShort } from 'react-icons/bs';

import Image from 'next/image';
const ProductCollection = () => {

  return (
    <div className='container mx-auto mb-10 mt-10 px-4'>
      <div className='flex-grow justify-center grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4'>
        <div className='flex items-center lg:justify-between justify-around bg-[#FFF0DC] rounded-[20px] p-1 mt-8'>
          <div className='p-4'>
            <h1 className='2xl:text-[29px] xl:text-[22px] lg:text-[20px] smd:text-[30px] text-[20px] font-bold'>
              Fresh Fruits <br />
              Collection
            </h1>
            <div className='rounded-full bg-white w-[60px] h-[60px] flex items-center justify-center mt-[70px]'>
              <i className='text-[#FF5C00] text-[30px]'>
                <BsArrowRightShort />
              </i>
            </div>
          </div>
          <div className=''>
            <Image
              src={img1}
              alt='image not found'
              className='object-cover'
            ></Image>
          </div>
        </div>
        <div className=' flex items-center lg:justify-between justify-around bg-[#DDF1D6] rounded-[20px] p-1 mt-8'>
          <div className='p-4'>
            <h1 className='2xl:text-[29px] xl:text-[22px] lg:text-[20px] smd:text-[30px] text-[20px] font-bold'>
              Vegetable <br />
              Collection
            </h1>
            <div className='rounded-full bg-white w-[60px] h-[60px] flex items-center justify-center mt-[70px]'>
              <i className='text-[#FF5C00] text-[30px]'>
                <BsArrowRightShort />
              </i>
            </div>
          </div>
          <div className=''>
            <Image
              src={img2}
              alt='image not found'
              className='object-cover'
            ></Image>
          </div>
        </div>
        <div className='flex items-center lg:justify-between justify-around md:col-span-2 lg:col-span-1 bg-[#FFEBB7] rounded-[20px] p-1 mt-8'>
          <div className='p-4'>
            <h1 className='2xl:text-[29px] xl:text-[22px] lg:text-[20px] text-[20px] smd:text-[30px] font-bold'>
              Grocery <br />
              Item
            </h1>
            <div className='rounded-full bg-white w-[60px] h-[60px] flex items-center justify-center mt-[70px]'>
              <i className='text-[#FF5C00] text-[30px]'>
                <BsArrowRightShort />
              </i>
            </div>
          </div>
          <div className=''>
            <Image
              src={img3}
              alt='image not found'
              className='object-cover'
            ></Image>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductCollection;
