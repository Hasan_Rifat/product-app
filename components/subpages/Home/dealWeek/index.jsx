import Image from 'next/image';
import dealShop from '../../../../public/images/dealShop.png';
import dealMan from '../../../../public/images/dealMan.png';

const DealWeek = () => {
  return (
    <div className='container mx-auto px-4'>
      <div className='bg-[#FAF4F0] flex items-center lg:justify-between justify-center p-6 rounded-xl flex-wrap flex-grow space-y-16'>
        <div className='w-[250px] xs:w-[250px] sm:w-[320px] lg:w-[350px]'>
          <Image
            src={dealShop}
            alt='image not found'
            className='object-cover'
          ></Image>
        </div>
        <div className='basis-[400px]'>
          <p className='text-center smd:text-[36px] text-2xl font-semibold -mt-4'>Deal of the Week</p>
          <div className='date flex justify-between items-center'>
            <div className='days'>
              <p className='text-[#FF5E4D] font-bold text-[25px] md:text-[46px]'>06</p>
              <p className='text-gray-500'>DAYS</p>
            </div>
            <p>:</p>
            <div className='hour'>
              <p className='text-[#FF5E4D] font-bold text-[25px] md:text-[46px]'>18</p>
              <p className='text-gray-500'>HOURS</p>
            </div>
            <p>:</p>
            <div className='min'>
              <p className='text-[#FF5E4D] font-bold text-[25px] md:text-[46px]'>33</p>
              <p className='text-gray-500'>MIN</p>
            </div>
            <p>:</p>
            <div className='sec'>
              <p className='text-[#FF5E4D] font-bold text-[25px] md:text-[46px]'>49</p>
              <p className='text-gray-500'>SEC</p>
            </div>
          </div>
          <div className='text-center'>
            <button className='text-center bg-[#27AE60] text-white rounded-full px-8 py-3 mt-6'>Shop Now</button>
          </div>
        </div>
        <div className='w-[300px] xs:w-[250px] sm:w-[350px] lg:w-[400px]'>
          <Image
            src={dealMan}
            alt='image not found'
            className='object-cover'
          ></Image>
        </div>
      </div>
    </div>
  );
};

export default DealWeek;
