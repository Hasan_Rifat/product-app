import Image from 'next/image';
import cs1 from '../../../../public/images/cs1.png';
import cs2 from '../../../../public/images/cs2.png';
import cs3 from '../../../../public/images/cs3.png';
import cs4 from '../../../../public/images/cs3.png';

const ProductInsurance = () => {
  return (
    <div className='container mx-auto mt-[100px] mb-[100px] px-4'>
      <div className='flex items-center justify-between flex-wrap flex-grow space-y-10'>
        <div className='cs1 flex items-center'>
          <Image
            src={cs1}
            alt='image not found'
            className='object-cover'
          ></Image>
          <div className="txt ml-6">
            <p className='font-semibold'>24 Customer Support</p>
            <p className='text-gray-400 text-sm mt-2'>Contact us 24 hours</p>
          </div>
        </div>
        <div className='cs1 flex items-center'>
          <Image
            src={cs2}
            alt='image not found'
            className='object-cover'
          ></Image>
          <div className="txt ml-6">
            <p className='font-semibold'>Authentic Products</p>
            <p className='text-gray-400 text-sm mt-2'>Contact us 24 hours</p>
          </div>
        </div>
        <div className='cs1 flex items-center'>
          <Image
            src={cs3}
            alt='image not found'
            className='object-cover'
          ></Image>
          <div className="txt ml-6">
            <p className='font-semibold'>Secure Payment</p>
            <p className='text-gray-400 text-sm mt-2'>Contact us 24 hours</p>
          </div>
        </div>
        <div className='cs1 flex items-center'>
          <Image
            src={cs4}
            alt='image not found'
            className='object-cover'
          ></Image>
          <div className="txt ml-6">
            <p className='font-semibold'>Bext Prices & Offers</p>
            <p className='text-gray-400 text-sm mt-2'>Contact us 24 hours</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductInsurance;
