import Layout from '../../../layouts';
import Banner from './Banner';
import Hero from './Banner';
import ProductSlide from './productSlide';
import Product from './productSlide';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ProductCollection from './productCollection';
import DealsProduct from './deals';
import DealsProductList from './dealsProduct';
import Brands from './brands';
import DealWeek from './dealWeek';
import ProductInsurance from './productInsurance';

const Home = () => {
  return (
    <div className=''>
      <Layout>
        <Hero />
        <ProductSlide />
        <ProductCollection />
        <DealsProductList/>
        <Brands/>
        <DealWeek/>
        <ProductInsurance/>
      </Layout>
    </div>
  );
};

export default Home;
