import DealsProduct from '../deals';
import img1 from '../../../../public/images/dealProduct1.png';
import img2 from '../../../../public/images/dealProduct2.png';
import img3 from '../../../../public/images/dealProduct3.png';
import img4 from '../../../../public/images/dealProduct4.png';

import { AiOutlineLeft } from 'react-icons/ai';
import { AiOutlineRight } from 'react-icons/ai';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Image from 'next/image';
import { useRef } from 'react';
import DealProductInfo from './DealProductInfo';

import { useSelector, useDispatch } from 'react-redux';

const DealsProductList = () => {
  const products = useSelector((state) => state.dealProductsReducer.products);
  // console.log('products', products);

  const sliderRef = useRef(null);

  const settings = {
    slidesToShow: 6,
    arrows: false,
    responsive: [
      {
        breakpoint: 411,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 540,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 640,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1280,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1400,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1920,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 4,
        },
      },
    ],
  };
  return (
    <div className='container mx-auto px-4'>
      <div className='flex justify-between items-center mb-4'>
        <div className=''>
          <h1 className='text-2xl font-bold'>Deals of the Week</h1>
        </div>
        <div className='flex justify-around mr-2'>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center mr-3'
            onClick={() => sliderRef.current.slickPrev()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineLeft />
            </i>
          </div>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center'
            onClick={() => sliderRef.current.slickNext()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineRight />
            </i>
          </div>
        </div>
      </div>
      <div className='relative'>
        <div className='border-2 border-green-500 w-[60px] rounded absolute top-0'></div>
        <div className='border border-gray-200 rounded'></div>
      </div>
      <div className=''>
        <Slider ref={sliderRef} {...settings}>
          {products?.map((product) => (
            <DealProductInfo
              key={product.id}
              product={product}
            ></DealProductInfo>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default DealsProductList;
