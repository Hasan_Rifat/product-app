import Modal from 'react-modal';
import { ImCross } from 'react-icons/im';
import Image from 'next/image';

import { AiFillStar } from 'react-icons/ai';
import { MdRemove } from 'react-icons/md';
import { IoMdAdd } from 'react-icons/io';
import { AiOutlineHeart } from 'react-icons/ai';
import { MdOutlineRepeat } from 'react-icons/md';
import { AiOutlineShareAlt } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import {
  addToCart,
  decrement,
  increment,
} from '../../../../../store/dealProductSlice';
import { useState } from 'react';

// import {
//   addToCart,
//   decrement,
//   increment,
// } from '../../../../store/dealProductSlice';

const customStyles = {
  content: {
    top: '15%',
    left: '25%',
    right: 'auto',
    bottom: '0',
    // marginRight: '-50%',
    // transform: 'translate(-50%, -50%)',
    zIndex: '10000 !important',
    backgroundColor: 'white',
    height: '600px',
    width: '50%',
  },
};

// if (this.state.viewport.width > 900) {
//   customStyles.content = { width: '45%', margin: '2.5%' };
// } else {
//   customStyles.content = { width: '100%', margin: '0' };
// }

const ProductDetailsModal = ({ setIsOpen, modalIsOpen, details }) => {
  const cart = useSelector((state) => state.dealProductsReducer.cart);
  const products = useSelector((state) => state.dealProductsReducer.products);
  // console.log('products of cart',products);
  const dispatch = useDispatch();
  const {
    id,
    productImg,
    productName,
    currentPrice,
    previousPrice,
    ratings5,
    numOfRatings,
    category,
    preSubCategory,
    subCategory,
    numOfCartProduct,
  } = details;

  const [numOfProduct, setNumOfProduct] = useState(numOfCartProduct);

  const handleClick = (id) => {
    console.log('clicked', id);
    setNumOfProduct(numOfCartProduct + 1);
    dispatch(increment(id));
  };
  function closeModal() {
    setIsOpen(false);
  }

  return (
    <div>
      <Modal
        isOpen={modalIsOpen}
        setIsOpen={setIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel='Example Modal'
        ariaHideApp={false}
      >
        <div className='grid grid-cols-12 gap-5 px-4'>
          <div className='col-span-5 basis-[100px]'>
            <Image
              width={320}
              height={320}
              // layout='fill'
              src={productImg}
              alt='image not found'
              className='object-cover'
            ></Image>
          </div>
          <div className='col-span-7 space-y-5 md:mr-10'>
            <div className='flex items-center space-x-8 relative'>
              <p className='uppercase text-sm text-gray-400'>status</p>
              <p className='uppercase text-sm text-green-400 font-semibold'>
                In Stock
              </p>
              <button className='text-md absolute right-0' onClick={()=>closeModal()}>
                <i>
                  <ImCross />
                </i>
              </button>
            </div>
            <div className='name'>
              <h1 className='text-xl font-semibold'>{productName}</h1>
            </div>
            <div className='flex space-x-5'>
              <div className='text-yellow-400 flex'>
                <i>
                  <AiFillStar />
                </i>
                <i>
                  <AiFillStar />
                </i>
                <i>
                  <AiFillStar />
                </i>
                <i>
                  <AiFillStar />
                </i>
                <i>
                  <AiFillStar />
                </i>
              </div>
              <div className=''>
                <p className='text-sm'>10 review</p>
              </div>
            </div>
            <div className='flex space-x-5 items-center'>
              <div className=''>
                <p className='font-semibold text-xl'>${currentPrice}</p>
              </div>
              <div className=''>
                <p className='line-through text-gray-400'>${previousPrice}</p>
              </div>
              <div className=''>
                <p className='text-gray-400 text-sm'>(+15% Vat Included)</p>
              </div>
            </div>
            <div className=''>
              <p className='text-gray-600 text-sm'>
                20 Products Sold in last 12 hours
              </p>
            </div>
            <div className=''>
              <hr />
            </div>
            <div className='flex justify-start items-center space-x-10'>
              <p className='uppercase'>quantity</p>
              <div className='flex space-x-5'>
                <button
                  className='rounded-full border p-3'
                  onClick={() => {
                    dispatch(decrement(id));
                  }}
                >
                  <i>
                    <MdRemove />
                  </i>
                </button>
                <button className='rounded-full border py-2 px-[18px] bg-[#F2F2F2]'>
                  {numOfCartProduct}
                </button>
                <button
                  className='rounded-full border p-3'
                  onClick={() => handleClick(id)}
                >
                  <i>
                    <IoMdAdd />
                  </i>
                </button>
              </div>
              <div className=''>
                <p className='text-gray-400 text-sm'>Only 10 items left!</p>
              </div>
            </div>
            <div className='bg-[#27AE60] p-3 rounded-full text-center text-white'>
              <button
                onClick={() => {
                  dispatch(addToCart(details));
                }}
              >
                Add to Cart
              </button>
            </div>
            <div className='bg-[#F2F2F2] p-3 rounded-full text-center text-stone-800'>
              <button>Buy Now</button>
            </div>

            <div className='flex items-center justify-between'>
              <div className='flex space-x-2 items-center'>
                <i>
                  <AiOutlineHeart />
                </i>
                <p className='text-sm text-gray-500'>Add to Wishlist</p>
              </div>
              <div className='flex space-x-2 items-center'>
                <i>
                  <MdOutlineRepeat />
                </i>
                <p className='text-sm text-gray-500'>Add to Compare</p>
              </div>
              <div className='flex space-x-2 items-center'>
                <i>
                  <AiOutlineShareAlt />
                </i>
                <p className='text-sm text-gray-500'>Share</p>
              </div>
            </div>
            <div className=''>
              <hr />
            </div>
            <div className='flex space-x-8'>
              <div className='text-gray-400 text-sm'>
                <p>SKU</p>
                <p>CATEGORY</p>
                <p>TAGS</p>
              </div>
              <div className='text-sm'>
                <p>KE-008819</p>
                <p>Food</p>
                <p>Food, Fruits, Vegetables</p>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ProductDetailsModal;
