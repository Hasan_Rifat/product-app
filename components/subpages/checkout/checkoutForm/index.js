import Link from 'next/link';

const CheckoutForm = () => {
  return (
    <>
      <div className='container mx-auto'>
        <div className='grid grid-cols-12 items-start pb-16 pt-4 gap-4 px-4'>
          <div className='grid lg:col-span-8 col-span-12 border border-green-200 p-4 rounded'>
            <h3 className='text-lg font-medium capitalize mb-4'>checkout</h3>
            <div className='space-y-4'>
              <div className='grid grid-cols-2 gap-4'>
                <div className=''>
                  <label htmlFor='text-gray-600 mb-2 block'>
                    First Name <span className='text-red-500'>*</span>
                  </label>
                  <input
                    type='text'
                    className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                  />
                </div>
                <div className=''>
                  <label htmlFor='text-gray-600 mb-2 block'>
                    Last Name <span className='text-red-400'>*</span>
                  </label>
                  <input
                    type='text'
                    className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                  />
                </div>
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>Company Name</label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>
                  Country/Region <span className='text-red-400'>*</span>
                </label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>
                  Street Address <span className='text-red-400'>*</span>
                </label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>
                  Town/City <span className='text-red-400'>*</span>
                </label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>Zip Code</label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>
                  Phone Number <span className='text-red-400'>*</span>
                </label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
              <div className=''>
                <label htmlFor='text-gray-600 mb-2 block'>
                  Email Address <span className='text-red-400'>*</span>
                </label>
                <input
                  type='text'
                  className='block w-full border border-fray-300 px-4 py-3 text-gray-600 text-sm rounded placeholder-gray-400 focus:border-green-400 focus:ring-0'
                />
              </div>
            </div>
          </div>
          <div className='grid lg:col-span-4 col-span-12 border border-gray-200 p-4 rounded'>
            <h4 className='text-gray-800 text-lg mb-4 font-semibold uppercase'>
              Order summary
            </h4>
            <div className='space-y-2'>
              <div className='flex justify-between'>
                <div className=''>
                  <h5 className='text-gray-800 font-medium'>
                    Italian shape sofa
                  </h5>
                  <p className='text-sm text-gray-600'>Size:M</p>
                </div>
                <p className='text-gray-600'>x3</p>
                <p className='text-gray-800 font-medium' $320></p>
              </div>
              <div className='flex justify-between'>
                <div className=''>
                  <h5 className='text-gray-800 font-medium'>
                    Italian shape sofa
                  </h5>
                  <p className='text-sm text-gray-600'>Size:M</p>
                </div>
                <p className='text-gray-600'>x3</p>
                <p className='text-gray-800 font-medium' $320></p>
              </div>
              <div className='flex justify-between'>
                <div className=''>
                  <h5 className='text-gray-800 font-medium'>
                    Italian shape sofa
                  </h5>
                  <p className='text-sm text-gray-600'>Size:M</p>
                </div>
                <p className='text-gray-600'>x3</p>
                <p className='text-gray-800 font-medium' $320></p>
              </div>
            </div>
            <div className='flex justify-between border-b border-gray-200 text-gary-800 font-medium my-3 uppercase'>
              <p>Subtotal</p>
              <p>$340</p>
            </div>
            <div className='flex justify-between border-b border-gray-200 text-gary-800 font-medium my-3 uppercase'>
              <p>Shipping</p>
              <p>$340</p>
            </div>
            <div className='flex justify-between text-gray-800 font-medium my-3 uppercase'>
              <p className='font-semibold'>Total</p>
              <p>$340</p>
            </div>

            <div className='flex items-center mb-4 mt-2'>
              <input
                type='checkbox'
                className='text-green-400 focus:ring-0 cursor-pointer rounded-sm w-3 h-3'
              />
              <label
                htmlFor='agreement'
                className='text-gray-600 ml-3 cursor-pointer text-sm'
              >
                Agree to our{' '}
                <a href='#' className='text-green-400'>
                  terms & condition
                </a>
              </label>
            </div>
            <Link href={'/'}>
              <a className='w-full block text-center bg-green-400 border-green-400 text-white md:p-4 smd:p-2 p-2 font-medium rounded-md hover:bg-transparent hover:bg-green-600'>
                Place order
              </a>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default CheckoutForm;
