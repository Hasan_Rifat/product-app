import React from 'react';
import Layout from '../../../layouts';
import CheckoutForm from './checkoutForm';

const CheckoutIndex = () => {
  return (
    <div>
      <Layout>
        <CheckoutForm />
      </Layout>
    </div>
  );
};

export default CheckoutIndex;
